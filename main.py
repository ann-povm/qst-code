#import os
# os.environ["TF_CPP_MIN_LOG_LEVEL"] = "0"
# os.environ["XLA_FLAGS"] = "--xla_hlo_profile"

#os.environ["CUDA_VISIBLE_DEVICES"] = str(int(os.environ["SLURM_PROCID"]) % 4)
#print(os.environ["CUDA_VISIBLE_DEVICES"])

import argparse

from flax import linen as nn
import matplotlib as mpl
import source as src
from source import binaryParamSearch
mpl.use('Pdf')

parser = argparse.ArgumentParser()

parser.add_argument('-id', help="Id", type=str, default="0")
parser.add_argument("-N", help="Particle No.", type=int, required=True)
parser.add_argument("-s", help="Train using s*4^N samples instead of using the exact distribution",
                    type=float, default=-1)
parser.add_argument("-Ns", help="Train using Ns samples instead of using the exact distribution",
                    type=float, default=-1)
parser.add_argument("-NsE", help="Evaluate using NsE samples instead of using the exact distribution",
                    type=float, default=2**19)
parser.add_argument("-net", help="Network architecture", type=str, default='CNN')
parser.add_argument('-r', "--repeat", help="Num. iterations to average over", type=int, default=1)
parser.add_argument("-f", help="Number of features per network layer", type=int, default=4)
parser.add_argument("-l", help="Number of network layers", type=int, default=2)
parser.add_argument("-k", help="Kernel size", type=int, default=4)
parser.add_argument("-e", "--encoding", help="Input encoding", choices=['image', 'one_hot'],
                    type=str, default='one_hot')
#parser.add_argument('--pretrain', help="Initialize nn params with a product state", action='store_true')
parser.add_argument("-lr", help="Learn rate", type=float, default=0.001)
parser.add_argument("-wd", help="Weight decay", type=float, default=0)
parser.add_argument("-beta1", help="Adam parameter", type=float, default=0.9)
parser.add_argument("-beta2", help="Adam parameter", type=float, default=0.999)
parser.add_argument("-gc", help="Gradient Clipping threshold", type=float, default=10**4)
parser.add_argument('-BS', "--batch_size", help="Batch size", type=int, default=-1)
parser.add_argument('-ME', "--max_epochs", help="Max number of epochs", type=int, default=6*10**3)
parser.add_argument('-HS', "--halt_slope", help="Slope of loss function at which to stop training",
                    type=float, default=-4*10**-6)
parser.add_argument('-HL', "--halt_loss", help="Value of loss function at which to stop training",
                    type=float, default=-100)
parser.add_argument("-saveDir", help="Output Directory", type=str, default=".")

parser.add_argument("--searchKey", help="Will optimize searchTargetKey by binary searching this parameter"
                                        " according to --searchParams", type=str, default='')
parser.add_argument("--searchTargetKey", help="Will optimize this parameter by binary searching searchKey"
                                              " according to --searchParams", type=str, default='finalInfidTrue')
parser.add_argument("--searchParams", help="Requires 4 arguments (min, max, max_steps, target_threshold).",
                    nargs=4, type=float)
parser.add_argument("--nameKeys", help="Keys in output file name", type=str)
parser.add_argument("--liveSamples", help="Compute training samples before starting (only use for small systems)",
                    type=int, default=0)
parser.add_argument("--avgDatasets", help="1: generate new samples for each repetition",
                    type=int, default=1)

parser.add_argument("-u", help="Uniform dephasing noise", type=float, default=0.0)

subparsers = parser.add_subparsers(help='Type of state to train', required=True, dest='type')

ising_parser = subparsers.add_parser('ising')
ising_parser.add_argument("-J", help="Ising coupling constant", type=float, default=1.0)
ising_parser.add_argument("-B", help="Ising external field", type=float, default=1.0)
ising_parser.add_argument("-d", "--dim", help="Dimension (1|2)", type=int, default=1)
ising_parser.add_argument("--alpha", help="Interaction exponent", type=float, default=None)


random_parser = subparsers.add_parser('random')
random_group = random_parser.add_mutually_exclusive_group(required=True)
random_group.add_argument('--mixed', help="Train a random density matrix", action='store_true')
random_group.add_argument('--pure', help="Train a random pure state density matrix", action='store_true')
random_group.add_argument('--purity', help="Train a random state with approx. uniform purity", action='store_true')

graph_parser = subparsers.add_parser('graph')
graph_group = graph_parser.add_mutually_exclusive_group(required=True)
#graph_group.add_argument('--nbonds', help='Number of bonds in the graph state', type=int, default=-1)
graph_group.add_argument('--cluster', help="Train an N particle cluster state", action='store_true')
graph_group.add_argument('--pCluster', help="Train an N particle cluster state with period boundary",
                         action='store_true')

ghz_parser = subparsers.add_parser('ghz')

mps_parser = subparsers.add_parser('cluster_mps')

experiment_parser = subparsers.add_parser('exp')

cnot_state_parser = subparsers.add_parser('cnot')

ensemble_parser = subparsers.add_parser('ensemble')
ensemble_parser.add_argument('--states', help="Path to ensemble of pure states", required=True)
ensemble_parser.add_argument('--samples', help='Path to samples', required=True)

args = parser.parse_args()

if args.batch_size == -1:
    batch_size = 4**args.N
else:
    batch_size = args.batch_size

searchParams = None
if args.searchKey != '':
    searchParams = args.searchKey, args.searchTargetKey, *args.searchParams


params = {
    'repeat': args.repeat,
    's': args.s,
    'Ns': args.Ns,
    'NsEval': args.NsE,
    'net': args.net,
    'N': args.N,
    'f': args.f,
    'l': args.l,
    'k': args.k,
    'encoding': args.encoding,
    'actFun': nn.tanh,
    'lr': args.lr,
    'wd': args.wd,
    'beta1': args.beta1,
    'beta2': args.beta2,
    'gc': args.gc,
    'halt_slope': args.halt_slope,
    'halt_loss': args.halt_loss,
    'batch_size': batch_size,
    'maxEpochs': args.max_epochs,
    'name_keys': args.nameKeys.replace(' ', '').split(','),
    'liveSamples': args.liveSamples,
    'avgDatasets': args.avgDatasets,
    'u': args.u,
    'id': args.id
}


if args.type == 'ising':
    params['J'] = args.J
    params['B'] = args.B
    params['dim'] = args.dim
    params['alpha'] = args.alpha


elif args.type == 'random':
    from source import trainRandomRho
    if args.mixed:
        params['random'] = 'any'

    elif args.pure:
        params['random'] = 'pure'

    elif args.purity:
        params['random'] = 'randomPurity'
    print('random', params['random'])
    trainRandomRho.learnRandom(params, doPrint=True, saveDir=args.saveDir)
    exit(0)

elif args.type == 'graph':

    #if args.nbonds == -1:
    #    args.nbonds = args.N
    #params['nbonds'] = args.nbonds

    if args.cluster:
        params['cluster'] = True

    if args.pCluster:
        params['pCluster'] = True


elif args.type == 'ghz' or args.type == 'cnot':
    pass

elif args.type == 'ensemble':
    params['states'] = args.states
    params['samples'] = args.samples

if searchParams is None:
    src.learnWithAverage(params, state=args.type, doPrint=True, saveDir=args.saveDir)
else:
    src.binaryParamSearch.binarySearch(params, args.type, searchParams, saveDir=args.saveDir)
