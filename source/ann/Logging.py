import numpy as np
from source.quantum import povms
from source import util, quantum as qu, dms
import matplotlib.pyplot as plt
import math

class Logger:
    def __init__(self, name, log_scale=False):
        self.name = name
        self.values = dict()
        self.errors = dict()
        self.log_scale = log_scale

    def add_value(self, source, value, error=0.):
        if source not in self.values:
            self.values[source] = []
            self.errors[source] = []
            self.newSeries(source)
        self.values[source][-1].append(float(value))
        self.errors[source][-1].append(error)

    def newSeries(self, source=None):
        if source is not None:
            self.values[source].append([])
            self.errors[source].append([])
        else:
            for source in self.values:
                self.values[source].append([])
                self.errors[source].append([])

    def log(self, source, distribution=None, samples=None):
        pass

    def plot(self, ax):
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

        max_len = 0
        for source in self.values:
            for series in range(len(self.values[source])):
                if len(self.values[source][series]) > max_len:
                    max_len = len(self.values[source][series])
        for i, source in enumerate(self.values):
            for series in range(len(self.values[source])):
                vals = np.array(self.values[source][series])
                if self.log_scale:
                    ax.set_yscale('log')
                if len(vals) == 1:
                    try:
                        ax.fill_between(range(max_len), [vals[0] - self.errors[source][series][0]] * max_len,
                                        [vals[0] + self.errors[source][series][0]] * max_len, alpha=0.3, color=colors[i])
                    except Exception as e:
                        print(e)

                    ax.plot([vals] * max_len, label=f'{source}', c=colors[i])
                else:
                    try:
                        ax.fill_between(range(len(vals)), vals - self.errors[source][series],
                                        vals + self.errors[source][series], alpha=0.3, color=colors[i])
                    except Exception as e:
                        print(e)
                    ax.plot(vals, label=f'{source}', c=colors[i])
        ax.set_ylabel(self.name)
        legend_without_duplicate_labels(ax)#ax.legend()

    def abs_error(self, source1, source2):
        return np.abs(np.array(self.values[source1])[:, -1] - np.array(self.values[source2])[:, -1])

    def rel_error(self, source1, source2):
        return self.abs_error(source1, source2)/np.array(self.values[source2])[:, -1]

    def mean_rel_error(self, source1, source2):
        rel_errors = self.rel_error(source1, source2)
        return np.mean(rel_errors), np.std(rel_errors)

    def toDict(self):
        res = dict()
        for source in self.values:
            self.values[source] = [x for x in self.values[source] if x != []]
            self.errors[source] = [x for x in self.errors[source] if x != []]
            res[source] = [float(lastNotNan(x)) for x in self.values[source]] #list(np.array(self.values[source])[:, -1])

            if self.errors[source]:
                res[source+"_err"] = [float(x[-1]) for x in self.errors[source]]
            else: res[source + "_err"] = []
        return res

    def __str__(self):
        return str(self.toDict())

    def relSlope(self, source):
        m, _ = util.getLastAvgSlope(self.values[source][-1])
        m /= self.values[source][-1][-1]
        return m


def lastNotNan(array):
    for i in range(len(array)):
        if np.isnan(array[-(i+1)]):
            pass
        else:
            return array[-(i+1)]
    return np.nan

def legend_without_duplicate_labels(ax):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique))

def plotLoggers(loggers, path):
    num = len(loggers)
    width = int(np.sqrt(num))
    height = math.ceil(num / width)
    fig, axs = plt.subplots(height, width, figsize=(15, 15))
    for i, evaluator in enumerate(loggers):
        evaluator.plot(axs.flat[i])
    fig.tight_layout()
    fig.savefig(path)

def avg_ensemble(pure_states, func):
    return np.mean([func(state) for state in pure_states])
    #return np.mean([joblib.Parallel(n_jobs=16)(joblib.delayed(func)(state) for state in pure_states)])

class ObservableLogger(Logger):
    def __init__(self, name: str, observable, sites, dephasing=0, povm=povms.default_povm):
        super().__init__(name)
        self.obs = observable
        self.dist_obs = povm.getPovmObservable(dms.opToTensorOp(observable))
        self.sites = sites
        self.dephasing = dephasing

    def log(self, source: str, distribution=None, samples=None, pure_states=None, samples_for_mle=None):
        N = None
        if distribution is not None: N = int(np.log(len(distribution))/np.log(4))
        elif samples is not None: N = len(samples[0])
        elif pure_states is not None: N = int(np.log(len(pure_states[0])) / np.log(2))
        elif samples_for_mle is not None: N = len(samples_for_mle[0])

        if self.sites is None:
            res = np.array([self.computeObs([i], distribution=distribution, samples=samples, pure_states=pure_states,
                                            samples_for_mle=samples_for_mle) for i in range(N)])
            val = np.mean(res[:, 0])
            err = np.mean(res[:, 1])
        else:
            val, err = self.computeObs(self.sites, distribution, samples, pure_states, samples_for_mle)
        self.add_value(source=source, value=val, error=err)

    def computeObs(self, sites, distribution=None, samples=None, pure_states=None, samples_for_mle=None):
        if distribution is not None:
            return povms.expVal(distribution, self.dist_obs, sites), 0
        elif samples is not None:
            val, err = povms.sampleObs(self.dist_obs, samples, sites)
            return val, err
        elif pure_states is not None:
            return np.mean(qu.expVal(self.obs, pure_states, sites, dephasing=self.dephasing)), 0
        elif samples_for_mle is not None:
            rho_subsystem = dms.subsystem_mle(samples_for_mle, sites, povms.default_povm)
            distribution = povms.default_povm.getPOVMDistribution(rho_subsystem, return_1D=False)
            return self.computeObs(list(range(len(sites))), distribution=distribution)

class EntanglementEntropyLogger(Logger):
    def __init__(self, num_sites, dephasing=0):
        self.num_sites = num_sites
        self.dephasing = dephasing
        name = f"{num_sites}-Spin Entanglement Entropy"
        super().__init__(name)

    def log(self, source, distribution=None, samples=None, pure_states=None, samples_for_mle=None):
        if distribution is not None:
            N = distribution.ndim
            dist_t = povms.partialTrace(distribution, range(self.num_sites, N)) # trace out particles num_sites, ..., N
            self.add_value(source, povms.entanglement_entropy(dist_t))
        elif pure_states is not None:
            dm_sub = avg_ensemble(pure_states, lambda state: qu.subsystem_dm(state, self.num_sites))
            rho = (1-self.dephasing)*dm_sub + self.dephasing*np.eye(2**self.num_sites)//2**self.num_sites
            self.add_value(source, dms.vonNeumannEntropy(rho))
        elif samples_for_mle is not None:
            rho_subsystem = dms.subsystem_mle(samples_for_mle, list(range(self.num_sites)), povms.default_povm)
            return self.add_value(source, dms.vonNeumannEntropy(rho_subsystem))

class PurityLogger(Logger):
    def __init__(self, num_sites, dephasing=0):
        self.num_sites = num_sites
        self.dephasing = dephasing
        name = f"{num_sites}-Spin Purity"
        super().__init__(name)

    def log(self, source, distribution=None, samples=None, pure_states=None, samples_for_mle=None):
        if distribution is not None:
            N = distribution.ndim
            dist = povms.partialTrace(distribution, range(self.num_sites, N)) # trace out particles num_sites, ..., N
            self.add_value(source, povms.purity(dist))
        elif samples is not None:
            pass
            #samples = np.array(samples)[:, :self.num_sites] #only keep samples from first num_sites sites
            #self.add_value(source, *povms.sample_purity(samples))
        elif pure_states is not None:
            val = avg_ensemble(pure_states,
                               lambda state: qu.subsystem_purity(state, num=self.num_sites, dephasing=self.dephasing))
            self.add_value(source, val)
        elif samples_for_mle is not None:
            rho_subsystem = dms.subsystem_mle(samples_for_mle, list(range(self.num_sites)), povms.default_povm)
            self.add_value(source, dms.purity(rho_subsystem))


class NegativityLogger(Logger):
    def __init__(self):
        super().__init__("Negativity")

    def log(self, source, distribution=None, samples=None, samples_for_mle=None):
        if distribution is not None:
            rho = povms.linRecon_t(distribution)
            self.add_value(source, dms.negativity(rho))
        elif samples is not None:
            print("Negativity from samples not implemented!")

class QuantumFidelityLogger(Logger):
    def __init__(self, target_distribution):
        super().__init__("Quantum Fidelity")
        self.rho_target = povms.linRecon(target_distribution)

    def log(self, source, distribution=None, samples=None, samples_for_mle=None):
        if distribution is not None:
            rho = povms.linRecon_t(distribution)
            self.add_value(source, dms.fidelity(rho, self.rho_target))
        elif samples is not None:
            print("Quantum Fidelity from samples not implemented!")
