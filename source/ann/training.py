import time

import flax
import jax
import numpy as onp
from jax import random, numpy as jnp
from functools import partial
from jax import jit
from source import util
import matplotlib.pyplot as plt

from source import ann
from source.ann import Networks

net = Networks.CNN

@jax.jit
def kl(P, Q):
    #print("P: " + str(P.shape))
    #print("Q: " + str(Q.shape))
    return jnp.nansum(P * jnp.log(P / Q))#jnp.nansum(Q * jnp.log(Q / P))
    #return jnp.sum((P-Q)**2)

@jax.jit
def fid(P, Q):
    return -jnp.sum(jnp.sqrt(P*Q))

@jit
def evaluate(params, data):
    return net().apply({'params': params}, data).squeeze()

@partial(jit, static_argnums=(3, 4,))
def train_step(optimizer, batch, target, simMeasure, total_target_len):
    """Train for a single step."""
    def loss_fn(params):
        res = net().apply({'params': params}, batch)
        res /= jnp.sum(res)/res.shape[0]*total_target_len
        res = res.squeeze()
        #print(target)
        #print(jax.device_get(res))
        #print(batch.shape)
        loss = simMeasure(target, res)/res.shape[0]*total_target_len
        #loss = -(jnp.sum(jnp.sqrt(target*res))/res.shape[0]*total_target_len)**2
        return loss, res
    
    grad_fn = jax.value_and_grad(loss_fn, has_aux=True)
    (loss, res), grad = grad_fn(optimizer.target)
    optimizer = optimizer.apply_gradient(grad)
    return optimizer, loss

@partial(jit, static_argnums=(3, 4, 6))
def train_epoch(optimizer, basis, target, simMeasure, batch_size, key, num_steps_per_epoch):

    def iterable(state, _):
        key, opt = state
        key, subkey = jax.random.split(key)
        batch_indices = jax.random.choice(subkey, len(target), shape=(batch_size,), replace=True)
        #print(jax.device_get(batch_indices))
        opt, metrics = train_step(opt, basis[batch_indices], target[batch_indices], simMeasure, len(target))
        return (key, opt), metrics

    if len(target) < batch_size:
        batch_size = len(target)
        num_steps_per_epoch = 1

    (_, optimizer), epoch_metrics = jax.lax.scan(iterable, (key, optimizer), None, length=num_steps_per_epoch)

    epoch_metrics = jnp.sum(epoch_metrics)
    
    if simMeasure == fid:
        return optimizer, epoch_metrics/num_steps_per_epoch+1
    else:
        return optimizer, epoch_metrics/num_steps_per_epoch


def learn(params, target, doPrint=False, return_losses=False, initial_params=None,
          return_nn_params=False):

    print(jax.devices())

    if jnp.min(target) < 10**-14:
        target += 10**-10
        target /= jnp.sum(target)

    N = params['N']

    losses = {
        'fid': fid,
        'kl': kl
    }

    nets = {
        'CNN': Networks.CNN,
        'CCNN': Networks.CCNN
    }
    global net
    net = nets[params['net']]

    lossFct = losses[params['loss']]

    batch_metrics = []
    fidsDist = []
    norms = []

    opt = params.get('opt', flax.optim.Adam)
    haltLoss = params.get('halt_loss', -100)
    maxEpochs = params.get('maxEpochs', 10 ** 5)
    halt_slope = params.get('halt_slope', 0)
    batch_size = params.get('batch_size', 4 ** N)
    num_steps_per_epoch = params.get('epoch_size', 4 ** N // batch_size)
    if batch_size > 4 ** N:
        print('Warning: Batch size > Num. data points')

    net.features = [params['f'] for i in range(params['l'])]
    net.kernels = [params['k'] for i in range(params['l'])]
    net.actFns = [params['actFun'] for i in range(params['l'])]
    net.encoding = params['encoding']

    optimizer, _ = ann.create_optimizer(net, N, opt, {'learning_rate': params['lr']}, seed=int(time.time() * 1000),
                                        initial_params=initial_params)

    #print(jax.tree_map(onp.shape, optimizer.target))

    print(f"\n{ann.getNumParams(optimizer)} parameters for {4 ** N} POVM elements"
          f" (Overfit = {ann.getNumParams(optimizer) / 4 ** N}).\n")
    print(f'Epoch\tLoss\t\tInFid\t\tKL-Slope')

    basis = ann.getBasis(N)
    key = random.PRNGKey(int(time.time()))

    start_time = time.time()

    # last_print_time = 0
    for i in range(maxEpochs):  # pb.progressbar(range(10000), redirect_stdout=True):

        # optimizer, metrics = Network.train_step(optimizer, basis, target, lossFct)
        key, subkey = jax.random.split(key)
        optimizer, metrics = train_epoch(optimizer, basis, target, lossFct, batch_size, subkey, num_steps_per_epoch)
        metrics = jax.device_get(metrics)

        # if time.time() - last_print_time > 3:
        if i % 100 == 0:
            # last_print_time = time.time()

            res = jax.device_get(evaluate(optimizer.target, basis))
            norm = onp.sum(res)
            norms.append(norm)
            res_np = res / norm

            fidDist = 1 - util.fidelity(res_np, target)

            if onp.isnan(fidDist):  # training instability -> makes no sense to continue
                break

            # m, _ = util.getLastAvgSlope(batch_metrics)
            m, _ = util.getLastAvgSlope(fidsDist)
            m /= fidDist

            if doPrint:
                print(f'{i}\t{metrics:.2e}\t{fidDist:.2e}\t{m:.2e}      ', end='\r')

            if i > 1000 and i % 1000 == 0 and m != onp.NaN and halt_slope != 0 and (m > halt_slope and m < 0):
                break

            if haltLoss != -100 and fidDist < haltLoss:
                break

        batch_metrics.append(metrics)
        fidsDist.append(fidDist)

    duration = time.time() - start_time
    if doPrint: print("\n")

    # fids[fids == -onp.inf] = onp.nan
    plotRun(batch_metrics, fidsDist)

    res = {
        #"config": params,
        "finalKL": float(jax.device_get(kl(res_np, target))),
        "finalInfid": fidDist,
        "numParams": ann.getNumParams(optimizer),
        "finalSlope": m,
        "nnDist": res_np.tolist(),
        "target": target.tolist(),
        "duration": duration,
        "numSteps": i
    }

    if return_losses:
        res['sampled_loss'] = batch_metrics
        res['true_loss'] = fidsDist
        res['norms'] = norms

    if return_nn_params:
        return res, optimizer.target
    else:
        return res


def plotRun(loss, distFid):
    #plt.subplot(1, 2, 1)
    #plt.scatter(range(len(loss)), loss, label="Loss")
    #util.getLastAvgSlope(loss, doPlot=True)
    #plt.yscale('log')
    #plt.legend()
    #plt.subplot(1, 2, 2)
    #plt.plot(fid, label="Qu. Infidelity")
    plt.plot(distFid, label="Infidelity")
    plt.ylabel('Infidelity')
    plt.xlabel('Steps')
    util.getLastAvgSlope(distFid, doPlot=True)
    plt.yscale('log')
    # plt.legend()
    # if savePlotName is None:
    #    plt.show()
    # else:
    #    plt.savefig(name + ".png")

