import numpy

from scipy import linalg as sp

from source import util
from source.util import safeReal
import source
import progressbar as pb

np = numpy

def partial_trace_t(rho_tensor, indices): #Todo: write unittest
    """
        Computes the partial trace of a density matrix, given in tensor notation (shape = (2, 2, ..., 2, 2, ...))
        over the indices given. Eg. for a 2-particle d.m. indices = [1] traces out the second particle and
        indices = [0, 1] computes the full trace.
    """
    rho_tensor = subtranspose(rho_tensor)
    N = rho_tensor.ndim // 2
    indices = np.array(indices)+1
    all_indices = np.arange(1, 2*N+1, dtype='int32')

    # eg for N = 4 and indices = [1, 4]: all_indices = [1, 2, 3, 4, 1, 6, 7, 4]
    all_indices[N + indices - 1] = all_indices[indices - 1]
    #print(all_indices.tolist())
    return subtranspose(np.einsum(rho_tensor, all_indices.tolist(), optimize=True, dtype=source.dtypeC))

def vonNeumannEntropy(rho):
    evals = np.linalg.eigvalsh(rho)
    non_zero = evals[evals > 0]
    non_zero /= np.sum(non_zero)
    return -np.sum(non_zero*np.log2(non_zero))


def tensorOpToOp(op_t):
    """
        Turns an operator indexed by single particle indices (shape = (2)**2N)
        into a (true) matrix operator (shape = (2**N, 2**N))
    """

    N = op_t.ndim // 2

    return op_t.reshape((2 ** N, 2 ** N))


def opToTensorOp(op):
    """
        Turns a (true) matrix operator (shape = (2**N, 2**N))
        into an operator indexed by single particle indices (shape = (2)**2N)
    """

    N = int(np.log2(op.shape[0]))

    return op.reshape(*([2]*2*N))

def subsystem_mle(samples, sites, povm):
    subsystem_samples = np.array(samples)[:, sites]
    dataset_distribution = util.getDatasetWeights(subsystem_samples, return_dist=True)[1]
    dataset_distribution = povm.tensorDistTo1D(dataset_distribution)
    return iMLE(dataset_distribution, povm, len(sites))


def iMLE(dataset_dist, povm, N, max_iter=100, init_rho=None):
    """
        Performs iterative maximum likelihood estimation of the
        density matrix (eg. https://arxiv.org/pdf/quant-ph/9806014.pdf).
    Parameters
    ----------
    dataset_dist:   1-D array containing the relative observed frequencies of all possible measurement outcomes
    povm:           instance of povms.POVM
    N:              no. of qubits
    max_iter:       max number of iMLE iterations to do
    init_rho:       initial guess of density matrix

    Returns         estimate of density matrix
    -------

    """
    if init_rho is None:
        #pure_state_guess = np.zeros(2 ** N)
        #pure_state_guess[0] = 1
        #init_rho = qu.pureStateToDM(pure_state_guess)
        init_rho = np.eye(2**N)/2**N

    povm_ops = povm.getNPtclePOVM(N)

    #last_infid = 1
    rho_guess = init_rho
    for i in range(max_iter):
        dist_guess = povm.getPOVMDistribution(rho_guess)#povms.getPauli4Dist(rho_guess, N)

        #infid = 1 - util.fidelity(dist_guess, dataset_dist)
        #if infid > last_infid:
        #    break
        #last_infid = infid
        #print(f'Iteration {i}: Guess infidelity: {infid:.2e}', end='\r')

        coeffs = dataset_dist / dist_guess
        #coeffs[np.isnan(coeffs)] = 0
        R = np.einsum('ijk,i->jk', povm_ops, coeffs)
        rho_guess = R @ rho_guess @ R
        rho_guess /= np.trace(rho_guess)
    #print('\n')
    return rho_guess

def fast_iMLE(dataset, povm, max_iter=100):
    """
    Same as iMLE, but with runtime complexity of O(len(dataset)*4**N) instead of O(16**N)
    Parameters
    ----------
    dataset: list of povm samples
    povm: povm object
    max_iter: max number of iterations

    Returns
    -------
    2**N x 2**N density matrix
    """

    dataset = np.array(dataset)[:, ::-1]

    N = len(dataset[0])

    rho = np.eye(2**N, dtype=np.complex128)/2**N

    ops = np.array([povm.sampleToOp(sample) for sample in dataset])

    for _ in pb.progressbar(range(max_iter)):
        R = np.zeros_like(rho)
        for op in ops:
            R += op/expVal(rho, op)

        rho = R @ rho @ R
        rho /= np.trace(rho)
    return rho


def subtranspose(tensor): #Todo: incorporate into einsum everywhere and remove function
    N = tensor.ndim//2
    out_shape = list(range(N))[::-1] + list(range(N, 2*N))[::-1]

    # ijklmn -> kjinml
    return np.einsum(tensor, range(2*N), out_shape)

def _binN(x, N):
    return [(x >> i) & 1 for i in range(N)]


def getRandomRho(size):
    re = numpy.random.random((size, size))
    im = numpy.random.random((size, size))

    matrix = re + 1j*im
    matrix = matrix@matrix.T.conj()
    matrix /= np.trace(matrix)

    return np.array(matrix, dtype=source.dtypeC)


def purity_t(rho_t):
    return expVal_t(rho_t, rho_t)


def expVal_t(op_t, rho_t):
    assert op_t.ndim == rho_t.ndim
    indices1 = list(range(rho_t.ndim // 2))
    indices2 = list(range(rho_t.ndim // 2, rho_t.ndim))
    return safeReal(np.einsum(op_t, indices1 + indices2, rho_t, indices2 + indices1, dtype=source.dtypeC))


def purity(rho):
    return expVal(rho, rho)

def negativity(rho):
    evals = np.linalg.eigvals(rho)
    evals = np.abs(evals[evals < 0])
    return np.sum(evals)


def expVal(op, rho):
    #return safeReal(np.trace(rho@op)) #equivaluent but less efficient
    return safeReal(np.einsum('ij,ji', op, rho, dtype=source.dtypeC))


def fidelity(rho1, rho2):
    temp = sp.sqrtm(rho1)
    return np.abs(np.trace(sp.sqrtm(temp@rho2@temp)))**2


def isPhysical(matrix):
    return isHermitean(matrix) and isPositive(matrix) and unitTrace(matrix)


def isPositive(matrix):
    eigenvalues, _ = np.linalg.eigh(matrix)
    return np.all(eigenvalues >= -source.errorMargin) and np.all(eigenvalues <= 1+source.errorMargin)


def unitTrace(matrix):
    return np.abs(np.trace(matrix) - 1) < source.errorMargin #np.isclose(np.trace(matrix), 1)


def isHermitean(matrix):
    return np.all(np.abs(matrix - matrix.conj().T) < np.array(source.errorMargin))
