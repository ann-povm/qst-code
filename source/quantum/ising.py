import numpy as np

from source import quantum as qu
from source import sampling
import scipy.linalg as sl
import scipy.sparse.linalg as sla
import os
import json

def loadState(J, B, N, dim=1, alpha=None):
    path = "../../data/states/isingLocal"
    path = os.path.join(os.path.dirname(__file__),
                        path)  # to access the folder no matter from where the function is called

    onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and ".npy" in f]

    for f in onlyfiles:
        json_name = f.replace(".npy", "").replace("\'", "\"").replace("=", ":")
        # print(json_name)
        param_dict = json.loads(json_name)
        if 'J' in param_dict and 'B' in param_dict and 'N' in param_dict:
            if param_dict['J'] == J and param_dict['B'] == B and param_dict['N'] == N:
                if param_dict.get('dim', 1) == dim and param_dict.get('alpha', None) == alpha:
                    return np.load(os.path.join(path, f))

    raise Exception(f"No state file found in {path} that matches {J=}, {B=}, {N=}, {dim=}, {alpha=}")


def loadSamples(J, B, N, num=None, dim=1, dephasing=0, alpha=None):
    samples = None
    file_name = None
    if dim == 1:
        try:
            file_name = str({'N': N, 'J': J, 'B': B}).replace(':', '=') + '.txt'
            samples = sampling.loadSamples(os.path.join('isingLocal', file_name), num=num, dephasing=dephasing)
        except FileNotFoundError:
            pass
    if dim > 1 or samples is None or samples == []:
        try:
            if alpha is not None:
                raise FileNotFoundError
            file_name = str({'N': N, 'J': J, 'B': B, 'dim': dim}).replace(':', '=') + '.txt'
            samples = sampling.loadSamples(os.path.join('isingLocal1000000-100', file_name), num=num, dephasing=dephasing)
        except FileNotFoundError:
            file_name = str({'N': N, 'J': J, 'B': B, 'dim': dim, 'alpha': alpha}).replace(':', '=') + '.txt'
            samples = sampling.loadSamples(os.path.join('isingLocal1000000-100_decorr', file_name), num=num,
                                           dephasing=dephasing)
    if samples is None or samples == []:
        raise Exception(f'Failed to load samples for {file_name} in folder samples/isingLocal!')
    return samples


def pureGroundState(J, B, N, dim=1, alpha=None):
    #Duration on laptop:
    # N = 20 -> 50s
    # N = 22 -> 4min

    if alpha is None:
        if dim == 1:
            H_ = H(J, B,  N)
        elif dim == 2:
            H_ = H_2D(J, B, N)
        else:
            raise Exception("Only 1D and 2D hamiltonians currently supported...")
    else:
        H_ = H_longrange(J, B, N, alpha)
    evals, evecs = sla.eigsh(H_, k=1, which="SA")
    return evecs[:, 0]

def computeGroundState(J, B, N, u=0, dim=1, alpha=None):
    pure_state = pureGroundState(J, B, N, dim=dim, alpha=alpha)

    rho = qu.pureStateToDM(pure_state)
    rho = (1-u)*rho + u * np.identity(rho.shape[0])/2**N

    return rho


def H_2D(J, B, N):
    L = int(np.sqrt(N))
    couplings = []
    for i in range(L):
        for j in range(L):
            index = i*L+j
            index_left = ((i + 1) % L) * L + j
            index_down = i * L + ((j + 1) % L)

            couplings += [(qu.liftOp(qu.sz, index, N, sparse=True) @ qu.liftOp(qu.sz, index_left, N, sparse=True))]
            couplings += [(qu.liftOp(qu.sz, index, N, sparse=True) @ qu.liftOp(qu.sz, index_down, N, sparse=True))]
    H_ = -J * sum(couplings) - B * qu.totalOp(qu.sx, N, sparse=True)

    return H_

def couplingTerm(i, j, op, N):
    return qu.liftOp(op, i, N, sparse=True) @ qu.liftOp(op, j, N, sparse=True)

def couplingDirection(direction, N):
    return sum([couplingTerm(i, i+1, qu.paulis[direction], N) for i in range(N-1)])

def fieldDirection(direction, N):
    return qu.totalOp(qu.paulis[direction], N, sparse=True)

def H(J, B, N):
    if type(J) != list:
        J = [0, 0, J]
    if type(B) != list:
        B = [B, 0, 0]

    coupling = sum([-J[i-1]*couplingDirection(i, N) for i in [1, 2, 3]])
    field = sum([-B[i-1]*fieldDirection(i, N) for i in [1, 2, 3]])

    return coupling + field + 10**-12 * fieldDirection(3, N)

def H_longrange(J, B, N, alpha):
    coupling = 0
    for i in range(N):
        for j in range(N):
            if i < j:
                coupling += couplingTerm(i, j, qu.sz, N)/np.abs(i-j)**alpha
    coupling *= -J
    field = -B*fieldDirection(1, N)
    return coupling + field + 10**-12 * fieldDirection(3, N)

